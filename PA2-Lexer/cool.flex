/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */
%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

/*
 *  Add Your own definitions here
 */

int oparen = 0; // with init.

bool try_push_char(char c);
int overflow_err();

%}

/*
 * Define names for regular expressions here.
 */

DARROW          =>
DIGIT           [0-9]
INTEGER         {DIGIT}+
TYPEID          [A-Z][a-zA-Z0-9_]*
OBJECTID        [a-z][a-zA-Z0-9_]*

%x STRING
%x STRINGERR
%x COMMENT

%%

 /*
  *  Nested comments
  */
<INITIAL>"(*"   {
                    BEGIN(COMMENT);
                    oparen = 1;
                }
<COMMENT>"(*"   { oparen ++; }
<COMMENT>.      {}
<COMMENT>\n     {
                    curr_lineno ++;
                }
<COMMENT><<EOF>>        {
                            BEGIN(INITIAL);
                            cool_yylval.error_msg = "EOF in comment";
                            return (ERROR);
                        }
<INITIAL,COMMENT>"*)"   {
                            if (oparen > 0) {
                                oparen --;
                                if (oparen == 0)
                                    BEGIN(INITIAL);
                            }
                            else {
                                cool_yylval.error_msg = "Unmatched *)";
                                return (ERROR);
                            }
                        }
<INITIAL>--.*           {}


 /*
  *  The multiple-character operators.
  */
{DARROW}		{ return (DARROW); }
"<-"            { return (ASSIGN); }
"<="            { return (LE); }

 /*
  *  The single-character operators and special symbols.
  */
";"             { return ';'; }
","             { return ','; }
":"             { return ':'; }
"{"             { return '{'; }
"}"             { return '}'; }
"("             { return '('; }
")"             { return ')'; }
"@"             { return '@'; }
"."             { return '.'; }
"+"             { return '+'; }
"-"             { return '-'; }
"*"             { return '*'; }
"/"             { return '/'; }
"~"             { return '~'; }
"<"             { return '<'; }
"="             { return '='; }

 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */
(?i:class)      { return (CLASS); }
(?i:if)         { return (IF); }
(?i:then)       { return (THEN); }
(?i:else)       { return (ELSE); }
(?i:fi)         { return (FI); }
(?i:let)        { return (LET); }
(?i:in)         { return (IN); }
(?i:inherits)   { return (INHERITS); }
(?i:loop)       { return (LOOP); }
(?i:pool)       { return (POOL); }
(?i:while)      { return (WHILE); }
(?i:case)       { return (CASE); }
(?i:esac)       { return (ESAC); }
(?i:of)         { return (OF); }
(?i:new)        { return (NEW); }
(?i:isvoid)     { return (ISVOID); }
t(?i:rue)       { cool_yylval.boolean = true; return (BOOL_CONST); }
f(?i:alse)      { cool_yylval.boolean = false; return (BOOL_CONST); }
(?i:not)        { return (NOT); }

 /*
  *  Int constants
  *
  */
{INTEGER}       { cool_yylval.symbol = inttable.add_string(yytext);
                  return (INT_CONST); }

 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */
\"              {
                    string_buf_ptr = string_buf; // string reset
                    BEGIN(STRING);
                }
<STRING>\\b     {
                    if (!try_push_char('\b'))
                        return overflow_err();
                }
<STRING>\\t     {
                    if (!try_push_char('\t'))
                        return overflow_err();
                }
<STRING>\\n     {
                    if (!try_push_char('\n'))
                        return overflow_err();
                }
<STRING>\\f     {
                    if (!try_push_char('\f'))
                        return overflow_err();
                }
<STRING>\\\0    {
                    BEGIN(STRINGERR);
                    cool_yylval.error_msg = "String contains null character";
                    return (ERROR);
                }
<STRING>\\.     {
                    if (!try_push_char(yytext[1]))
                        return overflow_err();
                }
<STRING>\\\n    {
                    if (!try_push_char('\n'))
                        return overflow_err();
                }
<STRING>\n      {
                    curr_lineno ++;
                    BEGIN(INITIAL);
                    cool_yylval.error_msg = "Unterminated string constant";
                    return (ERROR);
                }
<STRING><<EOF>>   {
                    BEGIN(INITIAL);
                    cool_yylval.error_msg = "EOF in string constant";
                    return (ERROR);
                }
<STRING>\0      {
                    BEGIN(STRINGERR);
                    cool_yylval.error_msg = "String contains null character";
                    return (ERROR);
                }
<STRING>\"      {
                    *string_buf_ptr = '\0';
                    BEGIN(INITIAL);
                    cool_yylval.symbol = stringtable.add_string(string_buf);
                    return (STR_CONST);
                }
<STRING>.       {
                    if (!try_push_char(yytext[0]))
                        return overflow_err();
                }
<STRINGERR>\n   { BEGIN(INITIAL); }
<STRINGERR>\"   { BEGIN(INITIAL); }
<STRINGERR>.    {}

 /*
  *  Identifiers
  *
  */
{TYPEID}        {
                    cool_yylval.symbol = idtable.add_string(yytext);
                    return (TYPEID);
                }
{OBJECTID}      {
                    cool_yylval.symbol = idtable.add_string(yytext);
                    return (OBJECTID);
                }

 /*
  *  White Space
  *
  */
[ \f\r\t\v]     {}
\n              { curr_lineno ++; }

 /*
  *  Invalid Character
  *
  */
.               {
                    cool_yylval.error_msg = yytext;
                    return (ERROR);
                }

%%

bool try_push_char(char c) {
    if (string_buf_ptr < string_buf + MAX_STR_CONST - 1) {
        *string_buf_ptr = c;
        string_buf_ptr ++;
        return true;
    }
    else {
        return false;
    }
}

int overflow_err() {
    BEGIN(STRINGERR);
    cool_yylval.error_msg = "String constant too long";
    return (ERROR);
}

